var pac12_north = ['Stanford', 'Oregon', 'Washington State', 'California', 'Washington', 'Oregon State']; 
var pac12_north_abbr = ['su', 'ou', 'wsu', 'cal', 'uw', 'osu'];
var pac12_conference = ['8-1', '7-2', '6-3', '4-5', '4-5', '0-9'];
var pac12_overall = ['12-2', '9-4', '9-4', '8-5', '7-6', '2-10'];

function myFunction(){
    var list = document.querySelectorAll("li");
    
    for(var i = 0; i < pac12_north.length; i++){
        list[i].id = pac12_north_abbr[i];
        
        var college = document.querySelectorAll("div.college");
        var conference = document.querySelectorAll("div.conference");
        var overall = document.querySelectorAll("div.overall");
        
        for(var j = 0; j < college.length; j++){
            college[j].textContent = pac12_north[j];
            conference[j].textContent = pac12_conference[j];
            overall[j].textContent = pac12_overall[j];
        }
    }
}