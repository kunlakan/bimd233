function Flight(airline, number, type, origin, destination, dep_time, arrival_time, arrival_gate){
    this.airline = airline;
    this.number = number;
    this.type = type;
    this.origin = origin;
    this.destination = destination;
    this.dep_time = dep_time;
    this.arrival_time = arrival_time;
    this.arrival_gate = arrival_gate;
    this.flightDuration = (function(){
        var arrival = new Date(arrival_time);
        var departure = new Date(dep_time);
        var duration = new Date(arrival.getTime() - departure.getTime());
        return duration.toTimeString().split(' ')[0];
    }());
}

var flight1 = new Flight("Alaska", "AK333", "B737", "KSEA", "KGEG", "May 5, 2016 18:00:00", "May 5, 2016 19:20:00", "C7");
var flight2 = new Flight("United", "U2001", "A320", "KORD", "NYC", "May 5, 2016 19:15:00", "May 5, 2016 22:10:00", "N17");
var flight3 = new Flight("Delta", "DAL", "MD90", "KORD", "KNYC", "May 5, 2016 19:15:00", "May 5, 2016 22:10:00", "N17");
var flight4 = new Flight("Delta", "DAL", "B764", "KATL", "GRU / SBGR", "May 5, 2016 22:12:00", "May 6, 2016 8:18:00", "A4");
var flight5 = new Flight("Delta", "DAL", "B764", "KATL", "KSFO", "May 5, 2016 18:34:00", "May 5, 2016 20:37:00", "F20");

var flight = [flight1, flight2, flight3, flight4, flight5];

var el = document.getElementById("mybody");

flight1 = "<tr id=\"row1\"><td>" + flight[0].airline + "</td><td>" + flight[0].number + "</td><td>" + flight[0].type + "</td><td>" + flight[0].origin + "</td><td>" + flight[0].destination + "</td><td>" + flight[0].dep_time + "</td><td>" + flight[0].arrival_time + "</td><td>" + flight[0].arrival_gate + "</td><td>" + flight[0].flightDuration + "</td></tr>";

flight2 = "<tr id=\"row2\"><td>" + flight[1].airline + "</td><td>" + flight[1].number + "</td><td>" + flight[1].type + "</td><td>" + flight[1].origin + "</td><td>" + flight[1].destination + "</td><td>" + flight[1].dep_time + "</td><td>" + flight[1].arrival_time + "</td><td>" + flight[1].arrival_gate + "</td><td>" + flight[1].flightDuration + "</td></tr>";

flight3 = "<tr id=\"row1\"><td>" + flight[2].airline + "</td><td>" + flight[2].number + "</td><td>" + flight[2].type + "</td><td>" + flight[2].origin + "</td><td>" + flight[2].destination + "</td><td>" + flight[2].dep_time + "</td><td>" + flight[2].arrival_time + "</td><td>" + flight[2].arrival_gate + "</td><td>" + flight[2].flightDuration + "</td></tr>";

flight4 = "<tr id=\"row2\"><td>" + flight[3].airline + "</td><td>" + flight[3].number + "</td><td>" + flight[3].type + "</td><td>" + flight[3].origin + "</td><td>" + flight[3].destination + "</td><td>" + flight[3].dep_time + "</td><td>" + flight[3].arrival_time + "</td><td>" + flight[3].arrival_gate + "</td><td>" + flight[3].flightDuration + "</td></tr>";

flight5 = "<tr id=\"row1\"><td>" + flight[4].airline + "</td><td>" + flight[4].number + "</td><td>" + flight[4].type + "</td><td>" + flight[4].origin + "</td><td>" + flight[4].destination + "</td><td>" + flight[4].dep_time + "</td><td>" + flight[4].arrival_time + "</td><td>" + flight[4].arrival_gate + "</td><td>" + flight[4].flightDuration + "</td></tr>";


el.innerHTML = flight1 + flight2 + flight3 + flight4 + flight5;