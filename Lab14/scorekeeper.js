var p1Button = document.querySelector("#p1");
var p2Button = document.getElementById("p2");
var resetButton = document.getElementById("reset");
var p1Display = document.querySelector("#p1Display");
var p2Display = document.querySelector("#p2Display");
var numInput = document.querySelector("input");
var winningScoreDisplay = document.querySelector("p span");
var allButton = document.querySelectorAll("button");
var p1Score = 0;
var p2Score = 0;
var gameOver = false;
var winningScore = 5;


for(var i = 0; i < allButton.length; i++){
    allButton[i].classList.add("btn", "btn-secondary");
}

p1Button.addEventListener("click", function() {
  if (!gameOver) {
    p1Score++;
    if (p1Score === winningScore) {
        p1Display.classList.add("winner");
        p1Button.classList.remove("btn-secondary");
        p1Button.classList.add("btn-success");
        gameOver = true;
    }
    p1Display.textContent = p1Score;
  }
});

p2Button.addEventListener("click", function() {
  if (!gameOver) {
    p2Score++;
    if (p2Score === winningScore) {
        p2Display.classList.add("winner");
        p2Button.classList.remove("btn-secondary");
        p2Button.classList.add("btn-success");
        gameOver = true;
    }
    p2Display.textContent = p2Score;
  }
});

resetButton.addEventListener("click", function() {
  reset();
});

function reset() {
    p1Score = 0;
    p2Score = 0;
    p1Display.textContent = 0;
    p2Display.textContent = 0;
    p1Display.classList.remove("winner");
    p2Display.classList.remove("winner");

    for(var i = 0; i < allButton.length - 1; i++){
        allButton[i].classList.remove("btn-success");
        allButton[i].classList.add("btn-secondary");
    }
    
    gameOver = false;
}

numInput.addEventListener("change", function() {
    if(this.value >= 0){
        winningScore = Number(this.value);
    }
    else{
        this.value = 0;
    }

    winningScoreDisplay.textContent = this.value;
 	reset();
});