var el = document.getElementById("state");

var state = "idle"

do{
    el.textContent = state;
    var cmd = prompt("Enter a command: ", "next");

    if(cmd === "next"){
        switch( state ){
            case "idle":
                state = "s1";
                break;
            case "s1":
                state = "s2";
                break;
            case "s2":
                state = "s3";
                break;
            case "s3":
                state = "idle";
                break;
        }
    }
    
    if( cmd === "exit" || cmd === "quit" ){
        break;
    }
    else{
        cmd = "next";
    }
}
while(cmd === "next");