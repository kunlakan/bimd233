var date = ["FRI, May 13", "SAT, May 14", "SUN, May 15", "MON, May 16", "TUE, May 17"];
var high = [82, 75, 69, 69, 68];
var low = [55, 52, 52, 48, 51];

var el = document.getElementById("mybody");

function getSum(total, num){
    return total + num;
}

for(var i = 0; i < date.length; i++){
    el.innerHTML += "<tr><td>" + date[i] + "</td><td>" + high[i] + "</td><td>" + low[i] + "</td></tr>";
}

el.innerHTML += "<tr><td colspan=\"3\"><b>Averange High Temperatures: </b>" + high.reduce(getSum) / high.length + "</td>";
el.innerHTML += "<td colspan=\"3\"><b>Averange Low Temperatures: </b>" + low.reduce(getSum) / low.length + "</td></tr>"; 