$('input').keypress(function(e) {
    var code = this.value;    
    if(e.which == 13){
        $("ul.list-group").append("<li class=\"list-group-item\">" + code + "</li>");
    }
});


$('ul').on("mouseover", "li", function() {
  $(this).attr('id', 'selected');
  $(this).text('Focused!');
});

$('ul').on("mouseleave", "li", function() {
  $(this).attr('id', 'deselected');
  $(this).text('Not focused');
});

$("button").click(function(){
    $("ul.list-group").empty();
})