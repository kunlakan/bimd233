var el = document.getElementById("mybody");

function Company(name, cap, sale, profit, employee){
    this.name = name;
    this.cap = cap;
    this.sale = sale;
    this.profit = profit;
    this.employee = employee;
}

function getData(item){
    el.innerHTML += "<tr><td>" + item.name + "</td><td>$" + item.cap + "</td><td>$" + item.sale + "</td><td>$" + item.profit + "</td><td>" + item.employee + "</td></tr>"
    
}

var MSFT = new Company("Microsoft", "381.7 B", "86.8 B", "22.1 B", 128000);
var SYA = new Company("Symetra Financial", "2.7 B", "2.2 B", "254.4 M", 1400);
var MU = new Company("Micron Technology", "37.6 B", "16.4 B", "3.0 B", 30400);
var FFIV = new Company("F5 Networks", "9.5 B", "1.7 B", "311.2 M", 3834);
var EXPE = new Company("Expedia", "10.8 B", "5.8 B", "398.1 M", 18210);
var NLS = new Company("Nautilus", "476 M", "274.4 M", "18.8 M", 340);
var HFWA = new Company("Heritage Financial", "531 M", "137.6 M", "21 M", 748);
var CSCD = new Company("Cascade Microtech", "239 M", "136 M", "9.9 M", 449);
var NKE = new Company("Nike", "83.1 B", "27.8 B", "2.7 B", 56500);
var ALK = new Company("Alaska Air Group", "7.9 B", "5.4 B", "605 M", 13952);

var companyList = [MSFT, SYA, MU, FFIV, EXPE, NLS, HFWA, CSCD, NKE, ALK];

