// weather update button click
$('button').on('click', function(e) {
  $('ul li').each(function() {
    console.log("this:" + this);
    $(this).remove();
  });
  $.ajax({
    url: "http://api.wunderground.com/api/f144d306c35d076e/geolookup/conditions/q/WA/Bothell.json",
    dataType: "jsonp",
    success: function(parsed_json) {
      var city = parsed_json['location']['city'];
      var state = parsed_json['location']['state'];
      var temp_f = parsed_json['current_observation']['temp_f'];
      var weather = parsed_json['current_observation']['weather'];
      var wind = parsed_json['current_observation']['wind_string'];
      var rh = parsed_json['current_observation']['relative_humidity']
      var img = weather.replace(/\s/g, '');
      img = img.toLowerCase();
      
      var str = "<li> Location : " + city + ", " + state + "</li>";
      $('ul').append(str);
      $('ul li:last').attr('class', 'list-group-item');
      var str = "<li> Current temperature is: " + temp_f + "</li>";
      $('ul').append(str);
      $('ul li:last').attr('class', 'list-group-item');
      var str = "<li> Relative Humidity is: " + rh + "</li>";
      $('ul').append(str);
      $('ul li:last').attr('class', 'list-group-item');
      var str = "<li> Weather is: " + weather + " <img src='http://icons.wxug.com/i/c/i/" + img + ".gif' width='25'></li>";
      $('ul').append(str);
      $('ul li:last').attr('class', 'list-group-item');
      var str = "<li> Current Wind Condition is: " + wind + "</li>";
      $('ul').append(str);
      $('ul li:last').attr('class', 'list-group-item');
      // console.log("Current temperature in " + location + " is: " + temp_f);
    }
  });
});
