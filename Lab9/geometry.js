function calcCircleGeometries(radius){
    const pi = Math.PI;
    var area = pi * radius * radius;
    var circumference = 2 * pi * radius;
    var diameter = 2 * radius;
    var geometries = [area.toFixed(2), circumference.toFixed(2), diameter.toFixed(2)];
    return geometries;
}

var radius = (Math.random()*100).toFixed(2);
var geometries = calcCircleGeometries(radius);

var el = document.getElementById("radius1");
el.textContent = "Radius: " + radius;
el = document.getElementById("area1");
el.textContent = "Area: " + geometries[0];
el = document.getElementById("circumference1");
el.textContent = "Circumference: " + geometries[1];
el = document.getElementById("diameter1");
el.textContent = "Diameter: " + geometries[2];

radius = (Math.random()*1000).toFixed(2);
geometries = calcCircleGeometries(radius);

el = document.getElementById("radius2");
el.textContent = "Radius: " + radius;
el = document.getElementById("area2");
el.textContent = "Area: " + geometries[0];
el = document.getElementById("circumference2");
el.textContent = "Circumference: " + geometries[1];
el = document.getElementById("diameter2");
el.textContent = "Diameter: " + geometries[2];

radius = (Math.random()*1000).toFixed(2);
geometries = calcCircleGeometries(radius);

el = document.getElementById("radius3");
el.textContent = "Radius: " + radius;
el = document.getElementById("area3");
el.textContent = "Area: " + geometries[0];
el = document.getElementById("circumference3");
el.textContent = "Circumference: " + geometries[1];
el = document.getElementById("diameter3");
el.textContent = "Diameter: " + geometries[2];