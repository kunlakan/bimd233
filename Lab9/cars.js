var car = [
  ["Audi", "R8", 2017, 162900.00],
  ["Jaguar", "F-Type", 2017, 69000.00],
  ["Mercedes-Benz", "C300 Sedan", 2016, 38950.00],
  ["Ferrari", "488GTB", 2016, 242737.00],
  ["Ford", "Mustang V6 Fastback", 2016, 24295.00]
];

var el = document.getElementById("row1");
el.innerHTML = "<td>" + car[0][0] + "</td><td>" + car[0][1] + "</td><td>" + car[0][2] + "</td><td>$" + car[0][3].toFixed(2) + "</td>";

el = document.getElementById("row2");
el.innerHTML = "<td>" + car[1][0] + "</td><td>" + car[1][1] + "</td><td>" + car[1][2] + "</td><td>$" + car[1][3].toFixed(2) + "</td>";

el = document.getElementById("row3");
el.innerHTML = "<td>" + car[2][0] + "</td><td>" + car[2][1] + "</td><td>" + car[2][2] + "</td><td>$" + car[2][3].toFixed(2) + "</td>";

el = document.getElementById("row4");
el.innerHTML = "<td>" + car[3][0] + "</td><td>" + car[3][1] + "</td><td>" + car[3][2] + "</td><td>$" + car[3][3].toFixed(2) + "</td>";

el = document.getElementById("row5");
el.innerHTML = "<td>" + car[4][0] + "</td><td>" + car[4][1] + "</td><td>" + car[4][2] + "</td><td>$" + car[4][3].toFixed(2) + "</td>";